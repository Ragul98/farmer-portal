# Farmer portal 


## API 
#### List places 
```json
{
    [
        {
            id : 1 , 
            place_name : "some place",
        },
        {
            id : 2 , 
            place_name : "other place",
        }
    ]

}
```

#### Farm list


```json
{
    {
        farm_name :"farm 1 "
        dev_id : [12,13,14]
    },
    {
        farm_name : "farm 2",
        dev_id :[12,13,123]
    }
}
```

#### Devices current status
```json
{
    [
{
        id : 1,
        type:"thermo",
        value:12
    }   ,
    {
        id:2 ,
        type:"motor",
        value:"ON"
    }
    {
        id:3,
        value: 41
    }]
}
```