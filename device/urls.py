"""farm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path 

from . import views


# urlpatterns = [ path('', views.index)
# ]
from django.urls import include, path, re_path
from rest_framework import routers
from device import views
from .views import DeviceList, DeviceStatusCreate
router = routers.DefaultRouter()
router.register (r'place' , views.devicePlaceView)
router.register(r'device', views.deviceView)
router.register(r'farms',views.PlaceFarmview)
# router.register(r'groups', views.GroupViewSet)


# Wire up our API using autogroupsmatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    re_path('^cust/(?P<id>.+)/$', DeviceList.as_view()),
    path('datapost/',DeviceStatusCreate.as_view()),
    path('view/', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]