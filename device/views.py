from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics
import django_filters.rest_framework
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

# Create your views here.

def index(request):
    return HttpResponse("hello world")


from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from device.serializers import UserSerializer, GroupSerializer, DeviceListSerializer, DeviceStatusCreateSerializer, DevicePlaceSerializer
from .models import *
from device.serializers import *
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer






# class DeviceListViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     id = self.kwargs["id"]
#     queryset = device.objects.filter(customer_id=id)
#     serializer_class = DeviceListSerializer


class DeviceList(generics.ListAPIView):
    serializer_class = DeviceListSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)

    def get_queryset(self):
        id = self.kwargs['id']
        return  device.objects.filter(customer_id=id)


class DeviceStatusCreate(generics.CreateAPIView):
    serializer_class = DeviceStatusCreateSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()

class devicePlaceGet(generics.ListAPIView):
    serializer_class = DevicePlaceSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    
    def get_queryset(self):
        id = self.kwargs['id']
        return  devicePlace.objects.filter(customer_id=id)


class devicePlaceView(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = [AllowAny]
    queryset = devicePlace.objects.all()
    serializer_class = DevicePlaceSerializer

class deviceView(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = [AllowAny]
    queryset = device.objects.all()
    serializer_class = deviceSerializer

class PlaceFarmview(viewsets.ModelViewSet):
    permission_classes = [AllowAny]
    queryset = PlaceFarm.objects.all()
    serializer_class = placefarmSerializer