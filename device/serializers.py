from django.contrib.auth.models import User, Group
from rest_framework import serializers

from device.models import *


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class deviceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta :
        model = device 
        fields = "__all__"

class DeviceListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = device
        fields = ['name','dev_type']


class DeviceStatusCreateSerializer(serializers.HyperlinkedModelSerializer):
    device_id = deviceSerializer(many =False ,read_only =True)
    class Meta:
        model = DeviceStatus
        fields = ['device_id','value']

class DevicePlaceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = devicePlace
        fields = ['place_name']



class placefarmSerializer(serializers.HyperlinkedModelSerializer):
    place_id = DevicePlaceSerializer(many=False,read_only=True)
    dev_id = deviceSerializer(many=True,read_only =True)
    class Meta :
        model = device 
        fields = "__all__"