from django.db import models

# Create your models here.

class device(models.Model):
    dev_type = models.CharField(max_length=10)
    name = models.CharField(max_length=20)
    customer_id = models.IntegerField()

    def __str__(self):
        return self.dev_type + str(self.customer_id)

class DeviceStatus(models.Model):
    device_id = models.ForeignKey(device,on_delete=models.CASCADE)
    timestamp = models.DateTimeField( auto_now_add=True)
    value = models.IntegerField()

    def __str__(self):
        return str(self.device_id) + str(self.tim)+str(value)

class devicePlace(models.Model):
    place_name = models.CharField(max_length=20)

    def __str__(self):
        return  str(self.place_name)

class PlaceFarm(models.Model):
    place_id = models.ForeignKey(devicePlace,on_delete =models.CASCADE)
    farm_name = models.CharField(max_length=10)
    dev_id = models.ManyToManyField(device)


    def __str__(self):
        return self.farm_name + str(self.place_id)